(ns chip2n.app
  (:require [reagent.core :as r]
            [goog.string :as string]))

(defonce click-count (r/atom 0))

(defn time->berlin-fmt
  "yields {:h5 2 :h1 0 :m5 6 :m1 1}"
  [t]
  (let [hours (.getHours t)
        mins  (.getMinutes t)
        sec   (.getSeconds t)
        fiveh (.floor js/Math (/ hours 5))
        fivem (.floor js/Math (/ mins 5))]
    {:h5 fiveh
     :h1 (- hours (* fiveh 5))
     :m5 fivem
     :m1 (- mins (* fivem 5))
     :s1 (even? sec)}))

(defonce db (r/atom {:time (js/Date.)}))

(defn clock [db]
  (let [{:keys [h5 h1 m5 m1 s1]} (time->berlin-fmt (:time @db))
        grid                     [[1 (if s1 1 0)] [4 h5] [4 h1] [11 m5] [4 m1]]]
    [:div {:style {:width "500px" :margin "25px auto"}}
     (for [[total on] grid]
       [:div {:style {:display :flex :justify-content :space-between}}
        (for [ix   (range total)
              :let [on? (< ix on)]]
          [:div {:style {:flex-grow  1
                         :background (if on? "red" "white")
                         :border     "1px solid darkgrey"}}
           (string/unescapeEntities "&nbsp;")])])]))

(defn render-app []
  (r/render [clock db]
            (.getElementById js/document "root")))

(defn update-time []
  (swap! db assoc :time (js/Date.)))

(defn init []
  (println "Initialization complete.")
  (render-app)
  (js/setInterval update-time 50))


(defn ^:dev/after-load start []
  (println "================ Loaded application =================")
  (render-app))


(defn ^:dev/before-load stop []
  (println "=============== Reloading application ==============="))
